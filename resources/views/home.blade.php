@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">My Stripe balance</div>

                <div class="panel-body">
                    <strong>My ID:</strong>: {{ auth()->user()->stripe->id }}<br>
                    <strong>My balance:</strong>: ${{ auth()->user()->stripe->account_balance }}<br>
                    <strong>Am I a delinquent:</strong>: {{ auth()->user()->stripe->delinquent ? 'yes!' : 'no…'}}<br>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">All users</div>

                <div class="panel-body">
                    <ul class="list-group">
                        @foreach($users as $user)
                            <li class="list-group-item">
                                <strong>Balance for {{ $user->email }}</strong>: ${{ $user->stripe->account_balance  }}
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Logs</div>

                <div class="panel-body">
                    <ul class="list-group">
                        @foreach(App\StripeQueryBuilder::$logs as $log)
                            <li class="list-group-item">
                                {{ $log }}
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
