<?php declare(strict_types = 1);

namespace App;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Stripe\Customer;

class StripeRelation extends Relation
{
    /**
     * @var StripeQueryBuilder
     */
    public $query;

    /**
     * @var string
     */
    public $localKey = 'stripe_id';

    public function __construct(Model $model)
    {
        parent::__construct(app(StripeQueryBuilder::class), $model);
    }

    /**
     * Set the base constraints on the relation query.
     *
     * @return void
     */
    public function addConstraints()
    {
        $this->query->id = $this->parent->{$this->localKey};
    }

    /**
     * Set the constraints for an eager load of the relation.
     *
     * @param  array $models
     * @return void
     */
    public function addEagerConstraints(array $models)
    {
        $this->query->ids = $this->getKeys($models, $this->localKey);
    }

    /**
     * Initialize the relation on a set of models.
     *
     * @param  array $models
     * @param  string $relation
     * @return array
     */
    public function initRelation(array $models, $relation)
    {
        return $models;
    }

    /**
     * Get the results of the relationship.
     *
     * @return mixed
     */
    public function getResults()
    {
        return $this->query->first();
    }

    /**
     * Match the eagerly loaded results to their parents.
     *
     * @param  array $models
     * @param  Collection $results
     * @param  string $relation
     * @return array
     */
    public function match(array $models, Collection $results, $relation)
    {
        foreach ($models as $model) {
            $model->setRelation($relation, $results[$model->{$this->localKey}]);
        }

        return $models;
    }
}