<?php declare(strict_types = 1);

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Mockery;
use Stripe\Customer;

class StripeQueryBuilder extends Builder
{
    const NUMBER_OF_CUSTOMERS_TO_FETCH = 2;
    public static $logs = [];

    /**
     * Stripe ID to fetch
     *
     * @var string
     */
    public $id;

    /**
     * @var string[]
     */
    public $ids = [];

    public function __construct()
    {
        parent::__construct(Mockery::mock(QueryBuilder::class));
    }

    /**
     * Fetch one Stripe customer with the API
     *
     * @param array $columns
     * @return Customer
     * @throws \Exception
     */
    public function first($columns = ['*'])
    {
        if ($this->id) {
            self::$logs[] = "One request to Stripe in order to fetch one customer with ID {$this->id}";
            return Customer::retrieve($this->id);
        }

        throw new \Exception("'first' method called with no ID provided.");
    }

    /**
     * Fetch Stripe customers with the API
     *
     * @param array $columns
     * @return Collection
     */
    public function get($columns = ['*'])
    {
        $models = new Collection;

        if ($this->ids) {
            $totalCount = count($this->ids);

            foreach ($this->fetchAllStripeCustomers() as $customer) {
                if (in_array($customer->id, $this->ids)) {
                    $models[$customer->id] = $customer;
                }

                if ($totalCount === $models->count()) {
                    // All models were found, no need to continue to fetch the customers.
                    break;
                }
            }
        }

        return $models;
    }


    /**
     * Lazy fetch the customers with a generator
     *
     * @return \Generator|Customer[]
     */
    private function fetchAllStripeCustomers()
    {
        do {
            self::$logs[] = "One request to Stripe in order to fetch " . self::NUMBER_OF_CUSTOMERS_TO_FETCH . " customers";
            $response = Customer::all([
                'limit' => self::NUMBER_OF_CUSTOMERS_TO_FETCH,
                'starting_after' => $lastYieldCustomer ?? null,
            ]);

            foreach ($response->data as $customer) {
                yield $customer;
                $lastYieldCustomer = $customer;
            }
        } while ($response->has_more);
    }
}