# Laravel Foreign Relations

Go check [my blog post](https://thibaud.dauce.fr/posts/2016-12-07-laravel-foreign-relations.html)!

## Installation

```bash
git clone https://framagit.org/ThibaudDauce/laravel-foreign-relations.git
cd laravel-foreign-relations
composer install
cp .env.example .env
```

Put your Stripe token in the `.env` file.

```bash
touch database/database.sqlite
php artisan migrate
php artisan serve
```

Register a few users and check the home page.

## Interesting files

##### Model — `app/User.php`

##### Relation — `app/StripeRelation.php`

##### Query Builder — `app/StripeQueryBuilder.php`

##### View — `resources/views/home.blade.php`

##### Controller — `app/Http/Controllers/HomeController.php`
